<?php
/*
Plugin Name: User Location
Plugin URI: https://bitbucket.org/rrgonzalez92/user-location-wp-plugin/src/master/
description: Set user location on a cookie
Version: 1.0
Author: Reinier Rodriguez Gonzalez
Author URI: https://www.linkedin.com/in/reinier-rodr%C3%ADguez-gonz%C3%A1lez-56253067/
License: GPL2
*/

add_action( 'init', 'save_user_location' );

function save_user_location () {
    echo "
        <script>
            const cookieName = 'wp-location';
            
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                const err = { msg: 'Geolocation is not supported by this browser.' };
                document.cookie = cookieName + '=' + JSON.stringify( err );
            }
            
            function showPosition(position) {
                const result = { 
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                }
                
                document.cookie = cookieName + '=' + JSON.stringify( result );
            }
            
            function showError(error) {
                let err;
                switch(error.code) {
                    case error.PERMISSION_DENIED:
                        err = { msg: 'User denied the request for Geolocation.' }
                        break;
                    case error.POSITION_UNAVAILABLE:
                        err = { msg: 'Location information is unavailable.' }
                        break;
                    case error.TIMEOUT:
                        err = { msg: 'The request to get user location timed out.' }
                        break;
                    default:
                        err = { msg: 'An unknown error occurred.' }
                        break;
                }
                
                document.cookie = cookieName + '=' + JSON.stringify( err );
            }
        </script>
    ";
}
