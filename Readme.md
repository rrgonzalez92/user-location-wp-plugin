# User Location on Cookie - Wordpress Plugin

## How to setup
1. Install docker and docker-compose
2. Run `docker-compose up -d` on source directory
3. Navigate to localhost on a web browser

## How to use plugin
1. Go to Wordpress plugins
2. Find the User Location plugin and Activate it
3. Reload website and allow it to get your location
4. Aproximate Coordinates will be stored on `user-location` COOKIE
